// Creating hotel database

// Inserting Single Room
db.rooms.insertOne({
	"name":"single",
	"accomodates":2,
	"price":1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available":10,
	"isAvailable":false
})

// Inserting multiple rooms
db.rooms.insertMany([
	{
		"name":"double",
		"accomodates":3,
		"price":2000,
		"description": "A room fit for a small family going on a vacation",
		"rooms_available":5,
		"isAvailable":false	
	},
	{
		"accomodates":4,
		"price":4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms_available":15,
		"isAvailable":false	
	}
])

// finding room with name "double"
db.rooms.find({"name":"double"})

// updating queen room
db.rooms.updateOne(
	{
		"accomodates":4
	},
	{
		$set:{
			"rooms_available":0
		}
	}
	)

// deleting rooms with 0 availability
db.rooms.deleteMany(
	{"rooms_available":0}
	)
